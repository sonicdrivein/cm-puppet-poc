class profile::ssh_server {
	package {'openssh-server':
		ensure => present,
	}
	service { 'sshd':
		ensure => 'running',
		enable => 'true',
	}
	ssh_authorized_key { 'root@master.puppet.vm':
		ensure => present,
		user   => 'root',
		type   => 'ssh-rsa',
		key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDD5JRDclpeHiO1TaT1oSRjP1u1D2efOvJULyWO0IPGfhUr8x2x05xnPZ+zSvL/mvlmjVmNIp21aru0fiPac9W5EjOFfYB59TEHLpNj7AXM8zwnPu/CPSuQv+GAcY62LwemzkCcgMEerlJUHOewX/ESvRR6RqXe3LQIOR+L6VWHdKH9ryMZE1XBfo5/Eylgj5gIovEQXYddzbDuvk4axwTh2dmfT7+ehYVYr12V9wSzFS2RDJ1pjIWvuwZzhCdjawjbDWXfwZqASc8YdpTJefmddRNBCuxYpD5Pww+E0B51uA5PvXxPeLbfxro8zf2aePVKnAx1EeJ+pYc6/hKZ9wY3',
	}  
}
